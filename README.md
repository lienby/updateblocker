# v1.1   
Fixed issue where Game Updates couldnt  be downloaded if "Block Update Download" was enabled   
(https://www.reddit.com/r/VitaPiracy/comments/ajibp7/cant_update_from_livearea/)   

Download (VPK):  
https://bitbucket.org/SilicaAndPina/updateblocker/downloads/UpdateBlocker-1.1.vpk  

# UpdateBlocker    

Features:   
  
1) Block "System Update" option in settings    
when this is enables the System Update tab in Settings wont allow you to complete an update.    
essentally blocking updates complete from here    
   
   
2) Block Update Downloads    
When this is enabled, update files cannot be automatically downloaded by the system     
(even if its enabled in auto-start settings) - the auto-start setting for system updates is also turned off. .    


Once enabled these features will work even without henkaku enabled.

The "Block Update Download" will be disabled upon restoring the system however,
and "Block System Update In Settings" will be disabled upon reinstalling the firmware.

In both cases, you can still use the update function in safe mode.   
so if you ever semi-brick its still recoverable...     
  
Download (VPK):  
https://bitbucket.org/SilicaAndPina/updateblocker/downloads/UpdateBlocker-1.1.vpk

