#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <vitasdk.h>

#include "ctrl.h"
#include "debugScreen.h"

#define printf psvDebugScreenPrintf
int ret;


int WriteFile(char *file, void *buf, int size) {
	SceUID fd = sceIoOpen(file, SCE_O_RDWR | SCE_O_CREAT, 0777);
	if (fd < 0)
		return fd;

	int written = sceIoWrite(fd, buf, size);

	sceIoClose(fd);
	return written;
}


int vshIoMount(int id, const char *path, int permission, int a4, int a5, int a6) {
  uint32_t buf[3];

  buf[0] = a4;
  buf[1] = a5;
  buf[2] = a6;

  return _vshIoMount(id, path, permission, buf);
}

void blockUpdates(){
	psvDebugScreenClear();

	ret = vshIoUmount(0x300, 0, 0, 0);
	printf("Unmount1 %x\n",ret);
	
	ret = vshIoUmount(0x300, 1, 0, 0);
	printf("Unmount2 %x\n",ret);
	
	ret = vshIoMount(0x300, NULL, 2, 0, 0, 0);
	printf("Mount vs0 RW %x\n",ret);
	//Remount vs0 with R/W Permissions.
	
	
	ret = sceIoRename("vs0:/app/NPXS10015/system_update_plugin.rco", "vs0:/app/NPXS10015/cute_girls_dying_plugin.rco.disabled");
	printf("sceIoRename = %x\n",ret);
	
	if(ret == 0x00){
		printf("-- Blocked \"System Update\" option successfully! ---\n");
		printf("Console rebooting in 5 seconds . . .\n");
		
		sceKernelDelayThread(5000000);
		scePowerRequestColdReset();
	}
	else
	{
		printf("Error %x\n",ret);
		sceKernelDelayThread(5000000);
		main();
	}
	
}

void unblockUpdates(){
	psvDebugScreenClear();

	ret = vshIoUmount(0x300, 0, 0, 0);
	printf("Unmount1 %x\n",ret);
	
	ret = vshIoUmount(0x300, 1, 0, 0);
	printf("Unmount2 %x\n",ret);
	
	ret = vshIoMount(0x300, NULL, 2, 0, 0, 0);
	printf("Mount vs0 RW %x\n",ret);
	//Remount vs0 with R/W Permissions.
	
	sceKernelDelayThread(1000);
	
	ret = sceIoRename("vs0:/app/NPXS10015/cute_girls_dying_plugin.rco.disabled","vs0:/app/NPXS10015/system_update_plugin.rco");
	printf("sceIoRename = %x\n",ret);
	
	if(ret == 0x00){
		printf("-- Unblocked \"System Update\" option successfully! ---\n");
		printf("Console rebooting in 5 seconds . . .\n");
		
		sceKernelDelayThread(5000000);
		scePowerRequestColdReset();
	}
	else
	{
		printf("Error %x\n",ret);
		sceKernelDelayThread(5000000);
		main();
	}
	
}

void blockDownloads(){
	psvDebugScreenClear();

	ret = sceRegMgrSetKeyInt("/CONFIG/NP2/AUTOUPDATE","system",0);
	printf("sceRegMgrSetKeyInt ret %x\n",ret);
	
	ret = sceIoRename("ur0:/bgdl/t","ur0:/bgdl/t.old");
	printf("sceIoRename ret %x\n",ret);
	
	char buf[0x1];
	memset(buf,0x00,0x1);
	ret = WriteFile("ur0:/bgdl/t",&buf,0x1);
	printf("WriteFile ret %x\n",ret);
	
	//No ur0:/bgdl folder = no automatic system update downloads :) 
	
	ret = sceIoRemove("ud0:/PSP2UPDATE/PSP2UPDAT.PUP");
	printf("sceIoRemove1 ret %x\n",ret);
	ret = sceIoRemove("ud0:/PSP2UPDATE/SYSTEMDATA.PUP");
	printf("sceIoRemove2 ret %x\n",ret);
	ret = sceIoRemove("ud0:/PSP2UPDATE/PREINST.PUP");
	printf("sceIoRemove3 ret %x\n",ret);
	ret = sceIoRemove("ud0:/PSP2UPDATE/PupInfo");
	printf("sceIoRemove4 ret %x\n",ret);
	
	printf("-- Update Downloads have been blocked! --\n");
	sceKernelDelayThread(5000000);
	main();
}


void unblockDownloads(){
	psvDebugScreenClear();

	ret = sceIoRemove("ur0:/bgdl/t");	
	printf("sceIoRemove ret %x\n",ret);
	ret = sceIoRename("ur0:/bgdl/t.old","ur0:/bgdl/t");
	printf("sceIoRename ret %x\n",ret);
	
	
	
	printf("-- Update Downloads have been unblocked! --\n");
	sceKernelDelayThread(5000000);
	main();
}


int main() {
		psvDebugScreenInit();
        psvDebugScreenClear(0);
		
		//Disable old Update Download Block
		//And enable new one
		sceIoRemove("ur0:/bgdl");	
		ret = sceIoRename("ur0:/bgdl.old","ur0:/bgdl");
		if(ret == 0)
		{
			blockDownloads();
		}
		

        psvDebugScreenPrintf("--- Vita Update Blocker ---\n");
        psvDebugScreenPrintf("CROSS: Block \"System Update\" in Settings\n");
        psvDebugScreenPrintf("CIRCLE: Unblock \"System Update\" in Settings\n");
        psvDebugScreenPrintf("SQUARE: Block update downloads\n");
        psvDebugScreenPrintf("TRIANGLE: Unblock update downloads\n");
        
        sceKernelDelayThread(100000);
        while(1)
        {
        switch(get_key(0)) {
                case SCE_CTRL_CROSS:
                    blockUpdates();
                    break;
                case SCE_CTRL_CIRCLE:
                    unblockUpdates();
                    break;
                case SCE_CTRL_SQUARE:
                    blockDownloads();
                    break;
                case SCE_CTRL_TRIANGLE:
                    unblockDownloads();
                    break;
                default:
                    break;
                }
        }
        

        
        return 0;
}
